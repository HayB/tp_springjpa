package com.formation.springTraining.repo;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.formation.springTraining.models.NotesEmp;
@Repository
public interface NotesEmpRepository   extends JpaRepository<NotesEmp, Integer>{

	
	public List<NotesEmp> findBySport(int sp);
	
	public Page<NotesEmp> findBySport(int sp, Pageable pageable);
	
	public List<NotesEmp> findBySocial(int soc);
	public Page<NotesEmp> findBySocial(int soc, Pageable pageable);

	public List<NotesEmp> findByPerformance(int perf);
	public Page<NotesEmp> findByPerformance(int perf, Pageable pageable);

	public List<NotesEmp> findByAssiduite(int assidu);
	public Page<NotesEmp> findByAssiduite(int assidu, Pageable pageable);

	public List<NotesEmp> findByMoyenne(int moy);
	public Page<NotesEmp> findByMoyenne(int moy, Pageable pageable);
	
	public List<NotesEmp> findByPeriode(LocalDate periode);
	public Page<NotesEmp> findByPeriode(LocalDate periode, Pageable pageable);

	
//	public NotesEmp findByLoginAndPassword(String l, String p);
	
	public Page<NotesEmp> findAll(Pageable pageable);

}
