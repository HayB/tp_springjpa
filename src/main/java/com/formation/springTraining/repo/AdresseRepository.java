package com.formation.springTraining.repo;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.formation.springTraining.models.Adresse;
import com.formation.springTraining.models.Employee;

public interface AdresseRepository extends JpaRepository<Adresse, Integer>{

	public List<Adresse> findByNumRue(int nr);
	
	public Page<Adresse> findByNumRue(int nr, Pageable pageable);
	
	public List<Adresse> findByRue(String r);
	
	public Page<Adresse> findByRue(String r, Pageable pageable);
	
	public List<Adresse> findByCp(String cp);
	
	public Page<Adresse> findByCp(String cp, Pageable pageable);
	
	public List<Adresse> findByVille(String v);
	
	public Page<Adresse> findByVille(String v, Pageable pageable);	
	
	public List<Adresse> findByPays(String p);
	
	public Page<Adresse> findByPays(String p, Pageable pageable);
	
//	public List<Adresse> findDistinctByRole(String role);
//	
//	public Employee findByLoginAndPassword(String l, String p);
//	
	public Page<Adresse> findAll(Pageable pageable);
//	
	
}
