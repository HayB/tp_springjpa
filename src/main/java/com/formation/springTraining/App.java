package com.formation.springTraining;

import java.time.LocalDate;
import java.util.List;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.formation.springTraining.config.AppConfig;
import com.formation.springTraining.config.JPAConfig;
import com.formation.springTraining.models.Adresse;
import com.formation.springTraining.models.Employee;
import com.formation.springTraining.models.NotesEmp;
import com.formation.springTraining.service.AdresseService;
import com.formation.springTraining.service.EmployeeService;
import com.formation.springTraining.service.NotesEmpService;

/**
 * Hello world!
 *
 */
public class App 
{
	 public static void main( String[] args )
	    {
	        System.out.println( "Hello World!" );
	      
	        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();	
	        context.register(AppConfig.class, JPAConfig.class);
	        context.refresh();
	        
	        EmployeeService empService = context.getBean(EmployeeService.class);   
//          List<Employee> empList = empService.findAll();
//	        Employee empLP = empService.findByLoginAndPassword("AJC", "ajc");
	        
//	        for (Employee employee : empList) {
//				System.out.println("Login = " + employee.getLogin() + " Password = " + employee.getPassword());
//			}
//	        
//	        System.out.println("User nom = " + empLP.getNom());
	        
	        Employee emp1 = context.getBean(Employee.class);
	        emp1.setNom("toto");
	        emp1.setPrenom("tata");
	        emp1.setLogin("tac");
	        emp1.setPassword("tic");
	        emp1.setEmail("tota@tato.fr");
	        emp1.setRole("tonton");
	        
// 		   	empList.add(emp1);
	        
	        empService.createOrUpdateEmployee(emp1);
	        
	        
	        
	       AdresseService adService = context.getBean(AdresseService.class);
	        
	       List<Adresse> adrList = adService.findAll(); 
	       Adresse adr1 = context.getBean(Adresse.class); 
	      
	        
	        
	        adr1.setNumRue(8);
	        adr1.setRue("rue des lilas");
	        adr1.setCp("75000");
	        adr1.setVille("paris");
	        adr1.setPays("FR");
	        adr1.setId_employee(emp1);
	        
//	        adrList.add(adr1);
	       
	        Adresse adr2 = context.getBean(Adresse.class); 
	      
	        
  			adr2.setNumRue(8);
	        adr2.setRue("rue des lilas");
	        adr2.setCp("75000");
	        adr2.setVille("paris");
	        adr2.setPays("FR");
	        adr2.setId_employee(emp1);
	        
//	        adrList.add(adr1);
	       
	        adService.createOrUpdateAdresse(adr1);
	        adService.createOrUpdateAdresse(adr2);
	        
	        
        
	        
	        NotesEmpService notesService = context.getBean(NotesEmpService.class);
	        
		    List<NotesEmp> notesList = notesService.findAll(); 
	        NotesEmp ntEmp = context.getBean(NotesEmp.class);
	        
	        ntEmp.setAssiduite(5);
	        ntEmp.setPerformance(7);
	        ntEmp.setSocial(3);
	        ntEmp.setSport(5);
	        ntEmp.setPeriode(LocalDate.of(2012, 02, 01));
	        ntEmp.getMoyenne();
	        ntEmp.setId_employee(emp1);
	        
	        NotesEmp ntEmp2 = context.getBean(NotesEmp.class);
	        
	        ntEmp2.setAssiduite(4);
	        ntEmp2.setPerformance(3);
	        ntEmp2.setSocial(3);
	        ntEmp2.setSport(10);
	        ntEmp2.setPeriode(LocalDate.of(2011, 02, 01));
	        ntEmp2.getMoyenne();
	        ntEmp2.setId_employee(emp1);
	        
//	        notesList.add(ntEmp);
	        
	        notesService.createOrUpdateNotesEmp(ntEmp);
	        notesService.createOrUpdateNotesEmp(ntEmp2);
	        
	        System.out.println(emp1.getNoteEmp() );
	        
	        emp1.setAdresseList(adrList);
	        emp1.setNoteEmp(notesList);

	        
    }
	 
}
	        
	        		

	        
        
	       
	        

    
    

