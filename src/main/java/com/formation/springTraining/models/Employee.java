package com.formation.springTraining.models;


import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
@Entity
@Table(name="Employee")
public class Employee {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	//@Column(name="employee_id")
	private int id;
	//@Column(name="employee_login")
	private String login;
	//@Column(name="employee_password")
	private String password;
	//@Column(name="employee_nom")
	private String nom;
	//@Column(name="employee_prenom")
	private String prenom;
	//@Column(name="employee_email")
	private String email;
	//@Column(name="employee_role")
	private String role;
	
	@Autowired
	@OneToMany(mappedBy="id_employee", cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
	private List<Adresse> adresseList;
	
	@Autowired
	@OneToMany(mappedBy="id_employee", cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
	private List<NotesEmp> noteEmp;
	


	public List<NotesEmp> getNoteEmp() {
		return noteEmp;
	}

	public void setNoteEmp(List<NotesEmp> noteEmp) {
		this.noteEmp = noteEmp;
	}

	

	public int getId() {
		return id;
	}
	

	public List<Adresse> getAdresseList() {
		return adresseList;
	}


	public void setAdresseList(List<Adresse> adresseList) {
		this.adresseList = adresseList;
	}


	public void setId(int id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}


	public void setLogin(String login) {
		this.login = login;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	public String getNom() {
		return nom;
	}


	public void setNom(String nom) {
		this.nom = nom;
	}


	public String getPrenom() {
		return prenom;
	}


	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getRole() {
		return role;
	}


	public void setRole(String role) {
		this.role = role;
	}



	public Employee(int id, String login, String password, String nom, String prenom, String email, String role) {
		this.id = id;
		this.login = login;
		this.password = password;
		this.nom = nom;
		this.prenom = prenom;
		this.email = email;
		this.role = role;
		
	}

	public Employee() {
		// TODO Auto-generated constructor stub
	}

}
