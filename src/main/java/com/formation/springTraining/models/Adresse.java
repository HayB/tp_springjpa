package com.formation.springTraining.models;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
@Entity
@Table(name="Adresse")
public class Adresse  {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	private int numRue;
	private String rue;
	private String cp;
	private String ville;
	private String pays;
	
	@ManyToOne
	@JoinColumn(name="id_employee", referencedColumnName="id")
	private Employee id_employee;
	
	
	
	
	public Employee getId_employee() {
		return id_employee;
	}

	public void setId_employee(Employee id_employee) {
		this.id_employee = id_employee;
	}
	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getNumRue() {
		return numRue;
	}

	public void setNumRue(int numRue) {
		this.numRue = numRue;
	}

	public String getRue() {
		return rue;
	}

	public void setRue(String rue) {
		this.rue = rue;
	}

	public String getCp() {
		return cp;
	}

	public void setCp(String cp) {
		this.cp = cp;
	}

	public String getVille() {
		return ville;
	}

	public void setVille(String ville) {
		this.ville = ville;
	}

	public String getPays() {
		return pays;
	}

	public void setPays(String pays) {
		this.pays = pays;
	}

	
	public Adresse(int numRue, String rue, String cp, String ville, String pays) {
		
		this.numRue = numRue;
		this.rue = rue;
		this.cp = cp;
		this.ville = ville;
		this.pays = pays;
	}


	public Adresse() {
		// TODO Auto-generated constructor stub
	}

}
