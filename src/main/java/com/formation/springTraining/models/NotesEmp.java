package com.formation.springTraining.models;

import java.time.LocalDate;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Entity
@Scope("prototype")
@Component
@Table(name="Notes")
public class NotesEmp {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id_notes;
	private int sport;
	private int social;
	private int performance;
	private int assiduite;
	private int moyenne;
	private LocalDate periode;
	
	@ManyToOne
	@JoinColumn(name="id_employee", referencedColumnName="id")
	private Employee id_employee;
	
	
	public Employee getId_employee() {
		return id_employee;
	}
	public void setId_employee(Employee id_employee) {
		this.id_employee = id_employee;
	}
	
	public int getId_notes() {
		return id_notes;
	}
	public void setId_notes(int id_notes) {
		this.id_notes = id_notes;
	}
	public int getSport() {
		return sport;
	}
	public void setSport(int sport) {
		this.sport = sport;
	}
	

	public int getSocial() {
		return social;
	}
	public void setSocial(int social) {
		this.social = social;
	}
	public int getPerformance() {
		return performance;
	}
	public void setPerformance(int performance) {
		this.performance = performance;
	}
	public int getAssiduite() {
		return assiduite;
	}
	public void setAssiduite(int assiduite) {
		this.assiduite = assiduite;
	}
	public int getMoyenne() {
		moyenne = (performance + sport + assiduite + sport)/4;
		
		return moyenne;
	}
	
	public LocalDate getPeriode() {
		return periode;
	}
	public void setPeriode(LocalDate periode) {
		this.periode = periode;
	}
	
	
	public NotesEmp() {
		super();
	}
	public NotesEmp(int sport, int social, int performance, int assiduite, int moyenne, LocalDate periode) {
		super();
		this.sport = sport;
		this.social = social;
		this.performance = performance;
		this.assiduite = assiduite;
		this.moyenne = moyenne;
		this.periode = periode;
	}

	
	
}
