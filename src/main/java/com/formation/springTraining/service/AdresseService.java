package com.formation.springTraining.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.formation.springTraining.models.Adresse;
import com.formation.springTraining.repo.AdresseRepository;

@Service
public class AdresseService {

	@Autowired
	private AdresseRepository adresseRepository;
	
	public void createOrUpdateAdresse(Adresse adr)
	{
		this.adresseRepository.save(adr);
	}
	
	public void deleteAdresse(Adresse adr)
	{
		this.adresseRepository.delete(adr);
	}

	public List<Adresse> findAll()
	{
		return this.adresseRepository.findAll();
	}
	public List<Adresse> findByNumRue(int nr){
		return this.adresseRepository.findByNumRue(nr);
	}
	
	public Page<Adresse> findByNumRue(int nr, Pageable pageable)
	{
		return this.adresseRepository.findByNumRue(nr, pageable);
	}
	
	public List<Adresse> findByRue(String r)
	{
		return this.adresseRepository.findByRue(r);
	}
	
	public Page<Adresse> findByRue(String r, Pageable pageable)
	{
		return this.adresseRepository.findByRue(r, pageable);
	}
	
	public List<Adresse> findByCp(String cp){
		return this.adresseRepository.findByCp(cp);
	
	}
	
	public Page<Adresse> findByCp(String cp, Pageable pageable)
	{
		return this.adresseRepository.findByCp(cp, pageable);
	}
	
	public List<Adresse> findByVille(String v)
	{
		return this.adresseRepository.findByVille(v);

	}
	
	public Page<Adresse> findByVille(String v, Pageable pageable)
	{
		return this.adresseRepository.findByVille(v, pageable);
	}
	
	public List<Adresse> findByPays(String p)
	{
		return this.adresseRepository.findByPays(p);

	}
	
	public Page<Adresse> findByPays(String p, Pageable pageable){
	return this.adresseRepository.findByPays(p, pageable);
	}
	
}
