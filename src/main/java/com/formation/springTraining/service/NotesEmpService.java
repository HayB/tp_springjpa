package com.formation.springTraining.service;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import com.formation.springTraining.models.NotesEmp;
import com.formation.springTraining.repo.NotesEmpRepository;
@Service
public class NotesEmpService {

	@Autowired
	private NotesEmpRepository notesRepository;
	
	public void createOrUpdateNotesEmp(NotesEmp noteEmp)
	{
		this.notesRepository.save(noteEmp);
	}
	
	public void deleteNotesEmp(NotesEmp noteEmp)
	{
		this.notesRepository.delete(noteEmp);
	}
	
	public List<NotesEmp> findAll()
	{
		return this.notesRepository.findAll();
	}
	
	public List<NotesEmp> findBySport(int sp)
	{
		return this.notesRepository.findBySport(sp);
	}

	public List<NotesEmp> findBySocial(int soc)
	{
		return this.notesRepository.findBySocial(soc);
	}

	public List<NotesEmp> findByPerformance(int perf)
	{
		return this.notesRepository.findByPerformance(perf);
	}

	public List<NotesEmp> findByAssiduite(int assidu)
	{
		return this.notesRepository.findByAssiduite(assidu);
	}

	public List<NotesEmp> findByMoyenne(int moy)
	{
		return this.notesRepository.findByMoyenne(moy);
	}

	public List<NotesEmp> findByPeriode(LocalDate periode)
	{
		return this.notesRepository.findByPeriode(periode);
	}

	

}
